include options.mk

.PHONY: all clean

CFLAGS=-Werror -Wall -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wredundant-decls
LDFLAGS=-lX11 -lXext -lXrender

all: ${NAME}

${NAME}: ${NAME}.o

${NAME}.o: ${NAME}.c

clean:
	rm -rf ${NAME} ${NAME}.o

install: all
	cp ${NAME} ${DESTDIR}${PREFIX}/bin

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/${NAME}

help:
	@echo -- ${NAME} Makefile help --
	@echo
	@echo No argument: Compile ${NAME}.
	@echo install: Installs ${NAME}. You may need to run this as root.
	@echo uninstall: Uninstalls ${NAME}. You may need to run this as root.
	@echo clean: Remove ${NAME}.o.

dist: clean
	mkdir -p ${NAME}-${VERSION}
	cp LICENSE Makefile *.c *.mk ${NAME}-${VERSION}
	tar -cf ${NAME}-${VERSION}.tar ${NAME}-${VERSION}
	gzip ${NAME}-${VERSION}.tar
	rm -rf ${NAME}-${VERSION}
